<?php

/**
 * @file
 * Views inc file for user Creator os browser info module.
 */

/**
 * Implements hook_views_data().
 */
function user_creator_system_details_views_data() {
  // The 'group' index will be used as a prefix in the UI for any of this
  // table's fields, sort criteria, etc. so it's easy to tell where they came
  // from.
  $data['user_creator_system_details_data']['table']['group'] = t('user creator system details');

  // This table references the {users} table. The declaration below creates an
  // 'implicit' relationship to the users table, so that when 'users' is the
  // base table, the fields are automatically available.
  $data['user_creator_system_details_data']['table']['join'] = array(
    // Index this array by the table name to which this table refers.
    // 'left_field' is the primary key in the referenced table.
    // 'field' is the foreign key in this table.
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );

  // creator_uid table field.
  $data['user_creator_system_details_data']['uid'] = array(
    'title' => t('content creator info'),
    'help' => t('Some example content that references a node.'),
    'relationship' => array(
      'base' => 'users',
      'base field' => 'uid',
      // 'field' => 'user' -- see hook_views_data_alter(); not needed here.
      'handler' => 'views_handler_relationship',
      'label' => t('User Creator'),
    ),
  );
  $data['user_creator_system_details_data']['browser'] = array(
    'title' => t('Creator browser'),
    'help' => t('Creator browser field.'),
    'filter' => array(
      'handler' => 'views_handler_filter',
    ),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
  );
  $data['user_creator_system_details_data']['os'] = array(
    'title' => t('Creator os'),
    'help' => t('Creator os field.'),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
  );
  $data['user_creator_system_details_data']['ip'] = array(
    'title' => t('Creator IP'),
    'help' => t('Creator IP field.'),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
  );
  return $data;
}
